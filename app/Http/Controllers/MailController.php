<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\ContactMe;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller {

	public function send(ContactMe $mail) {
		Mail::to('evanling@tuta.io')->send(new ContactMe($mail));
	}
}
