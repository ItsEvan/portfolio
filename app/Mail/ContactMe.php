<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMe extends Mailable {
	use Queueable, SerializesModels;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct() {

	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build(Request $request) {
		return $this->from('evanling@evanling.me')
			->view('emails.contact-me')
			->with([
				'name' => $request->name,
				'email' => $request->email,
				'company' => $request->company,
				'body' => $request->body,
			]);
	}
}
