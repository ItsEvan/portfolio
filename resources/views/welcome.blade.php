<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/index_critical.min.css') }}">
    {{ header("Access-Control-allow-Origin: http://portfolio.test") }}
    <title>Evan Ling</title>
</head>
<body>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <div id="app"></div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
