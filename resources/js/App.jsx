import React, { Component } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import Home from "./components/Home.jsx";
import Background from "./components/Background.jsx";
import Skills from "./components/Skills.jsx";
import Contact from "./components/Contact.jsx";

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isHomeLoaded: false,
			isInfoLoaded: false,
			showContact: false
		};
		this.showHomePage = this.showHomePage.bind(this);
		this.showInfoPage = this.showInfoPage.bind(this);
		this.toggleContact = this.toggleContact.bind(this);
	}

	showInfoPage() {
		this.setState({ isHomeLoaded: false, isInfoLoaded: true });
	}
	showHomePage() {
		this.setState({ isHomeLoaded: true, isInfoLoaded: false });
	}
	toggleContact() {
		this.setState({ showContact: !this.state.showContact });
	}

	render(location) {
		const { isHomeLoaded, isInfoLoaded, showContact } = this.state;
		return (
			<div>
				<TransitionGroup>
					<CSSTransition
						timeout={1000}
						appear={true}
						classNames="body"
						key={this.props.location.key}
					>
						<main>
							<Switch location={this.props.location}>
								<Route
									exact
									path="/"
									render={props => (
										<Home
											{...props}
											isHomePage={this.showHomePage}
										/>
									)}
								/>
								<Route
									path="/background"
									render={props => (
										<Background
											{...props}
											isInfoPage={this.showInfoPage}
										/>
									)}
								/>
								<Route
									path="/skills"
									render={props => (
										<Skills
											{...props}
											isInfoPage={this.showInfoPage}
										/>
									)}
								/>
							</Switch>
							<CSSTransition
								in={showContact}
								timeout={1000}
								classNames="fade"
								unmountOnExit
							>
								<Contact
									showContact={this.state.showContact}
									toggleContact={this.toggleContact}
								/>
							</CSSTransition>
						</main>
					</CSSTransition>
				</TransitionGroup>
				<nav>
					<CSSTransition
						in={isInfoLoaded}
						timeout={1000}
						classNames="nav"
						appear={isInfoLoaded}
						unmountOnExit
					>
						<Link onClick={this.showHomePage} to="/">
							<div className="nav__logo-container">
								<h1 className={"nav__logo-container__logo"}>
									el:
								</h1>
							</div>
						</Link>
					</CSSTransition>
					<div className="links-container">
						<div onClick={this.showInfoPage}>
							<Link className="link" to="/background">
								background
							</Link>
						</div>
						<div onClick={this.showInfoPage}>
							<Link className="link" to="/skills">
								skills
							</Link>
						</div>
						<div>
							<p className="link" onClick={this.toggleContact}>
								contact me
							</p>
						</div>
					</div>
				</nav>
			</div>
		);
	}
}

ReactDOM.render(
	<Router>
		<Route path="/" component={App} />
	</Router>,
	document.querySelector("#app")
);
