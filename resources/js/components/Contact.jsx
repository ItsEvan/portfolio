import React, { Component } from "react";
import ReactDOM from "react-dom";
import { CSSTransition } from "react-transition-group";

class Contact extends Component {
	constructor(props) {
		super(props);
		this.state = {
			name: "",
			email: "",
			company: "",
			body: "",
			emailError: "",
			showConfirm: ""
		};
		this.closeContact = this.closeContact.bind(this);
		this.updateForm = this.updateForm.bind(this);
		this.submitForm = this.submitForm.bind(this);
		this.validateForm = this.validateForm.bind(this);
	}

	closeContact() {
		this.props.toggleContact();
	}

	validateForm() {
		return /^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*$/.test(
			this.state.email
		);
	}

	updateForm() {
		this.setState({
			name: document.querySelector("#name").value,
			email: document.querySelector("#email").value,
			company: document.querySelector("#company").value,
			body: document.querySelector("#body").value
		});
	}

	submitForm() {
		if (this.validateForm()) {
			let form = document.querySelector("#form");
			let confirm = document.querySelector("#confirm");
			form.className = "form--fade";
			setTimeout(() => {
				confirm.className = "confirm--fade-in";
			}, 500);
			setTimeout(() => {
				this.props.toggleContact();
			}, 2000);
			setTimeout(() => {
				form.className = "form";
				confirm.className = "confirm";
			}, 2500);

			axios
				.post("/contact/send", {
					name: this.state.name,
					email: this.state.email,
					company: this.state.company,
					body: this.state.body
				})
				.then(response => {
					console.log(response);
				})
				.catch(error => {
					console.log(error.message);
				});
		} else {
			document.querySelector("#email").value = "";
			document.querySelector("#email").placeholder =
				"Please enter a valid email!";
		}
	}

	render() {
		const { showConfirm } = this;
		return (
			<div className="form-position">
				<div onClick={this.closeContact} className="background-shade" />
				<div className="contact-me">
					<div id="form" className="form">
						<h3>Aww, thanks!</h3>
						<div
							onClick={this.closeContact}
							className="close-button"
						>
							X
						</div>
						<div className="form-group">
							<label htmlFor="name">Name:</label>
							<input
								id="name"
								type="text"
								name="name"
								onChange={this.updateForm}
							/>
						</div>
						<div className="form-group">
							<label htmlFor="email">Email:</label>
							<input
								id="email"
								type="text"
								name="email"
								onChange={this.updateForm}
							/>
						</div>
						<div className="form-group">
							<label htmlFor="company">Company:</label>
							<input
								id="company"
								type="text"
								name="company"
								onChange={this.updateForm}
							/>
						</div>
						<div className="form-group">
							<label htmlFor="body">What's on your mind?</label>
							<textarea
								id="body"
								type="textarea"
								name="body"
								onChange={this.updateForm}
							/>
						</div>
						<button onClick={this.submitForm} id="send-mail">
							Send
						</button>
					</div>
					<div id="confirm" className="confirm">
						<h3>Message sent!</h3>
					</div>
				</div>
			</div>
		);
	}
}

export default Contact;
