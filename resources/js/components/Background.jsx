import React, { Component } from "react";
import ReactDOM from "react-dom";

class Background extends Component {
	componentDidMount() {
		this.props.isInfoPage();
	}
	render() {
		return (
			<div>
				<p>I'm a humble web developer based in Brockport, New York.</p>
				<p>
					Just trying to make a name for myself, and websites for
					others.
				</p>
				<p>
					I've done a little bit of everything, from server management
					and back-end logic to front-end development and design.
				</p>
				<p>Lessons learned hard are lessons hard forgotten.</p>
			</div>
		);
	}
}

export default Background;
