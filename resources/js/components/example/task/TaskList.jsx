import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class TaskList extends Component {

	constructor(props) {
		super(props);
		this.displayTask = this.displayTask.bind(this);
	}

	displayTask(task) {
		return <li key={task}>{task}</li>
	};

	render() {
		return (
			<ul>
				{ this.props.items.map(this.displayTask) }
			</ul>
			)
	}
}
export default TaskList;