import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class TaskForm extends Component {

	constructor(props) {
		super(props);
		this.addTask = this.addTask.bind(this);
		this.onChange = this.onChange.bind(this);
		this.state = {
			task: ''
		};
	}

	onChange(e) {
		this.setState({
			task: e.target.value
		});
	}

	addTask(e) {
		e.preventDefault();
		this.props.taskToAdd(this.state.task);
		
	}

	render() {

		return (
			<form onSubmit={this.addTask} className="form-group">
				<input onChange={this.onChange} value={this.state.task}/>
				<button>Add Task</button>
			</form>
			)
	}
}

export default TaskForm;