import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TaskList from './TaskList.jsx';
import TaskForm from './TaskForm.jsx';

class TaskApp extends Component {

	constructor(props) {
		super(props);
		this.state = {
			items: ['Go to store']
		};
		this.TaskFormHandler = this.TaskFormHandler.bind(this);
	}

	TaskFormHandler(task) {
		this.setState({
			items: this.state.items.concat([task])
		})
	}

	render() {
		return (
			<div>
			<h1>My Tasks</h1>
			<TaskList items={this.state.items} />
			<TaskForm taskToAdd={this.TaskFormHandler}/>
			</div>
			);
	}
}

ReactDOM.render(<TaskApp />, document.getElementById('TaskApp'));