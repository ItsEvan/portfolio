import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Gist extends Component {

	render() {

		return(
			<div>
				{this.props.username}'s last Gist is <a href={ this.props.url }>here</a>
			</div>
			);

	}
}

export default Gist;