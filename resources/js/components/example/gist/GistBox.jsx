import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Gist from './Gist.jsx';
import GistForm from './GistForm.jsx';
import axios from 'axios';

class GistBox extends Component {

	constructor(props) {
		super(props);
		this.state = {
			gists: [ 
			{
				username: '',
				url: ''
			}
			]
		};
		this.addGist = this.addGist.bind(this);
	}

	addGist(username) {
		// github doesn't allow csrf-token header
		let headers = window.axios.defaults.headers;
		delete headers.common["X-CSRF-TOKEN"];

		console.log(this.state.gists);
		var url = `https://api.github.com/users/${username}/gists`;
		axios.get(url)
		.then((response) => {
			console.log(response);
			let username = response.data[0].owner.login;
			let url = response.data[0].html_url;
			console.log(this.state.gists);
			let gists = this.state.gists.concat({ username, url });
			this.setState({ gists });
		});
	}


	render() {

		let newGist = function(gist) {
			return <Gist username={gist.username} key={gist.username} url={gist.url} />
		}
		return(
			<div>
			<h1>GistBox</h1>

			<GistForm onAdd={this.addGist}/>

			{ this.state.gists.map(newGist) }
			</div>
			)
	}
}

ReactDOM.render(<GistBox />, document.querySelector('#app'));