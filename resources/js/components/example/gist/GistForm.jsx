import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class GistForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			username: ''
		};
		this.onChange = this.onChange.bind(this);
		this.addGist = this.addGist.bind(this);
	}
	onChange(e) {
		this.setState({
			username: e.target.value
		});
	}
	addGist(e) {
		e.preventDefault();
		this.props.onAdd(this.state.username);
		this.setState({ username: '' });
	}
	render() {
		return(
			<div>
			<form onSubmit={this.addGist}>
			<input 
				value={this.state.username} 
				onChange={this.onChange} 
				placeholder="Type a github username..." 
				/>
			<button>Fetch latest Gist</button>
			</form>
			</div>
			)
	}

}

export default GistForm;