import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Counter extends Component {

    constructor(props) {
        super(props);
        this.state = {
            count: 5
        }
        this.add = this.add.bind(this);
        this.subtract = this.subtract.bind(this);
    }

    add() {
        this.setState((state) => ({
            count: state.count + 1
        }));
    }

    subtract() {
        this.setState((state) => ({
            count: state.count - 1
        }));
    }

    render() {
        return (
            <div>
            <h1>Counter: { this.state.count }</h1>

            <button onClick={this.subtract}>Subtract 1</button>
            <button onClick={this.add}>Add 1</button>
            </div>
            );
    }
};

ReactDOM.render(<Counter />, document.getElementById('example'));