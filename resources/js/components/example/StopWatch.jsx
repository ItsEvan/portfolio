import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class StopWatch extends Component {

	constructor(props) {
		super(props);
		this.state = {
			time: 0,
			until: 0
		};
		this.type = this.type.bind(this);
		this.start = this.start.bind(this);
		this.tick = this.tick.bind(this);
		this.isTimeUp = this.isTimeUp.bind(this);
		this.finish = this.finish.bind(this);
	}

	type(e) {
		this.setState({
			until: e.target.value
		})
	}

	start() {
		this.interval = setInterval(() => {
			this.tick();

			if (this.isTimeUp()) {
				this.finish();
			}
		}, 1000);
	}

	tick() {
		this.setState({
			time: this.state.time + 1
		});
	}

	isTimeUp() {
		return this.state.time == this.state.until;
	}

	finish() {
		console.log('Ding ding');
		this.setState({
			time: 0,
			until: ''
		});

		ReactDOM.findDOMNode(this.refs.input).focus();&
		
		return clearInterval(this.interval);
	}

	render() {
		return(
			<div>
			<h1>Timer</h1>
			<input ref="input" onChange={this.type} value={this.state.until}/>
			<button onClick={this.start}>Go</button>
			<h1>{this.state.time}</h1>
			</div>
			);
	}


}
ReactDOM.render(<StopWatch />, document.querySelector('#app'));