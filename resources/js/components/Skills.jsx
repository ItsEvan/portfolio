import React, { Component } from "react";
import ReactDOM from "react-dom";

class Skills extends Component {
	componentDidMount() {
		this.props.isInfoPage();
	}

	render() {
		return (
			<div>
				<h3>Jack of all trades</h3>
				<ul>
					<li>React</li>
					<li>Laravel</li>
					<li>ES6 JavaScript</li>
					<li>PHP 7</li>
					<li>Apache server management</li>
				</ul>
				<p>
					Other technologies: Webpack, Python, Bash Shell Scripting,
					Figma, SASS, Vagrant, Vue
				</p>
			</div>
		);
	}
}

export default Skills;
