import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Home extends Component {

	constructor(props) {
		super(props)
	}

	componentDidMount() {
		this.props.isHomePage();
	}

	componentWillUnmount() {
	}

	render() {
		return(
			<div className="intro">
				<div className={ "logo-container" }>
					<h1 className={ "logo-container" + "__logo" }>el:</h1>
				</div>
				<h1>Evan Ling</h1>
				<h2>Full Stack Web Developer</h2>
			</div>
			)
	}

}

export default Home;