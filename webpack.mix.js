const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
require('laravel-mix-critical');

mix.react('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
   .critical({
	   enabled: mix.inProduction(),
	   urls: [
		   { src: 'http://evanling.me', dest: 'public/css/index_critical.min.css' },
	   ],
	   options: {
		   minify: true,
	   },
   });

mix.browserSync({
	proxy: 'portfolio.test'
});
